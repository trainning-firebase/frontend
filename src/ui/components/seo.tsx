import Head from "next/head";

interface Props {
  title: string;
  description: string;
}

const Seo = ({ title, description }: Props) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <link rel="icon" href="/borne.png" />
      </Head>
    </div>
  );
};

export default Seo;
